from flask import Flask, render_template, redirect, url_for , session , logging
from flask_mysqldb import MySQL
from wtforms import form, StringField,TextAreaField , PasswordField , validators
from passlib.hash import sha256_crypt


app = Flask(__name__)

app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='password'
app.config['MYSQL_DB']='airlines'
app.config['MYSQL_CURSORCLASS']='DictCursor'

mysql =MySQL(app)


@app.route('/')
def index():
    return render_template('index.html', static_url_path='/static', static_folder='/static')

@app.route('/login')
def login():
    return render_template('login.html', static_url_path='/static', static_folder='/static' )

@app.route('/Promo')
def promo():
    return render_template('promo.html', static_url_path='/static', static_folder='/static')
@app.route('/booking')
def booking():
    return render_template('booking.html', static_url_path='/static', static_folder='/static')

@app.route('/signup', method =['GET' ,'POST'] )
def signup():
    form =RegisterForm(request.form)
    if request.form = 'POST' and form.validate():
        name=form.name.data
        email=form.email.data
        phonenumber=form.phonenumber.data



    return render_template('signup.html', static_url_path='/static', static_folder='/static' )

class RegisterForm(Form):
    name = StringField('name')
    email =  StringField('email')
    phonenumber = StringField('Phonenumber')
    password = PasswordField('Password', [validators.DataRequired(), validators.Equalto('Confirm', message= 'password does not match')])
    confirm = PasswordField('Confirm Password ')
if __name__ == '__main__':
    app.run(debug=True)
